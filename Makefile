##
## Makefile for BBB_pruss
##
## Made by Pierre Surply
## <pierre.surply@gmail.com>
##
## Started on  Fri Jan 31 14:13:56 2014 Pierre Surply
## Last update Tue Feb  4 21:11:06 2014 Pierre Surply
##

CROSS	?= arm-angstrom-linux-gnueabi-
IP	?= 192.168.103.160

CC	= $(CROSS)gcc
CFLAGS	= -static -I./drv/include -I./asm/ -MMD -std=c99
LDFLAGS = -lpthread
SCP	= scp

OBJDIR	= build
SRCDIR	= src
DRVLIB	= drv/lib

DTS	= BB-PRU-00A0.dts

SRC	= main.c	\
	  exec.c	\
	  data_mem.c	\
	  fibo.c	\
	  acker.c	\
	  gpio.c
DRV	= prussdrv.c

OBJ	= $(SRC:.c=.o) $(DRV:.c=.o)
DEP	= $(SRC:.c=.d) $(DRV:.c=.d)
OBJ	:= $(addprefix $(OBJDIR)/,$(OBJ))
DEP	:= $(addprefix $(OBJDIR)/,$(DEP))
SRC	:= $(addprefix $(SRCDIR)/,$(SRC))
SRC	:= $(addprefix $(DRVDIR)/,$(DRV))

EXEC	= bbb_pruss
DISTDIR	= bbb_pruss

all:: $(EXEC)

asm::
	make -C $@

$(EXEC): asm $(OBJDIR) $(OBJ)
	$(CC) $(CFLAGS) -o $@ $(OBJ) $(LDFLAGS)

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	$(CC) $(CFLAGS) -c -o $@ $<

$(OBJDIR)/%.o: drv/lib/%.c
	$(CC) $(CFLAGS) -c -o $@ $<

build:
	mkdir -p $@

upload:: $(EXEC)
	$(SCP) $(EXEC) $(DTS) root@192.168.103.160:~/bbb_pruss

clean::
	make -C asm clean
	$(RM) -r $(OBJDIR) $(EXEC)

-include $(DEP)
