/*
** exec.h for BBB_pruss
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Mon Feb  3 13:16:18 2014 Pierre Surply
** Last update Mon Feb  3 13:19:10 2014 Pierre Surply
*/

#ifndef EXEC_H
# define EXEC_H

# include "test.h"

void pruss_exec(const struct pruss_test *test);

#endif /* !EXEC_H */
