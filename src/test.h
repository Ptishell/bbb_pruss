/*
** test.h for BBB_pruss
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Mon Feb  3 13:04:57 2014 Pierre Surply
** Last update Tue Feb  4 14:38:19 2014 Pierre Surply
*/

#ifndef TEST_H
# define TEST_H

# include <stdlib.h>
# include <stdio.h>

# define LOG(FORMAT, ...)        printf(FORMAT, ## __VA_ARGS__)

struct pruss_test
{
  const char            *id;
  const unsigned int    *prgm_pru0;
  size_t                prgm_pru0_size;
  const unsigned int    *prgm_pru1;
  size_t                prgm_pru1_size;
  int                   (*check)(void);
};

#endif /* !TEST_H */
