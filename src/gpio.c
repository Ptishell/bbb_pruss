/*
** gpio.c for BBB_PRUSS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Tue Feb  4 18:13:43 2014 Pierre Surply
** Last update Mon Feb 10 19:01:38 2014 Pierre Surply
*/

#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <prussdrv.h>
#include <pruss_intc_mapping.h>

#include "test.h"

int counter = 0;
int quit = 0;

int check_gpio(void)
{
  int *mem;

  prussdrv_map_prumem(PRUSS0_PRU0_DATARAM, (void *) &mem);

  sleep(5);
  LOG("Counter: %d\n", *mem);

  return 0;
}

void alarm_handler(int sig)
{
  LOG("Counter: %d\n", counter);
  quit = 1;
}

int check_gpio_sysfs(void)
{
  int fd = open("/sys/class/gpio/gpio46/value", O_RDONLY);
  char prev;
  char buffer = 0;

  if (fd < 0)
    return 1;

  signal(SIGALRM, alarm_handler);
  alarm(5);

  quit = 0;
  while (!quit)
    {
      prev = buffer;
      lseek(fd, 0, SEEK_SET);
      read(fd, &buffer, 1);
      if (buffer == '1' && prev == '0')
        ++counter;
    }

  return 0;
}

#define GPIO1_ADDR      0x4804C000
#define GPIO_DATAIN     (0x138)

int check_gpio_mmap(void)
{
  int fd = open("/dev/mem", O_RDWR);
  int *gpio = mmap(NULL, 4096, PROT_READ | PROT_WRITE,
                    MAP_SHARED, fd, GPIO1_ADDR);
  char prev;
  char buffer = 0;

  if (fd < 0 || gpio < 0)
    return 1;

  signal(SIGALRM, alarm_handler);
  alarm(5);

  quit = 0;
  while (!quit)
    {
      prev = buffer;
      buffer = (gpio[GPIO_DATAIN / 4] >> 14) & 1;
      if (buffer == 1 && prev == 0)
        ++counter;
    }

  return 0;
}
