/*
** acker.c for BBB_PRUSS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Tue Feb  4 14:36:09 2014 Pierre Surply
** Last update Tue Feb  4 14:49:10 2014 Pierre Surply
*/

#include <prussdrv.h>
#include <pruss_intc_mapping.h>

#include "test.h"

int check_acker(void)
{
  int *mem;

  LOG("Waiting interrupt\n");
  prussdrv_pru_wait_event(PRU_EVTOUT_0);
  prussdrv_pru_clear_event(PRU_EVTOUT_0, PRU0_ARM_INTERRUPT);

  prussdrv_map_prumem(PRUSS0_PRU0_DATARAM, (void *) &mem);

  if (*mem == 125)
    return 0;

  return *mem;
}
