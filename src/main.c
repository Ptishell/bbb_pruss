/*
** main.c for BBB_pruss
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Fri Jan 31 14:12:55 2014 Pierre Surply
** Last update Mon Feb 10 18:38:24 2014 Pierre Surply
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <err.h>
#include <prussdrv.h>
#include <pruss_intc_mapping.h>

#include <42_bin.h>
#include <fibo_bin.h>
#include <acker_bin.h>
#include <gpio_bin.h>

#include "test.h"
#include "exec.h"

int check_data_mem(void);
int check_fibo(void);
int check_acker(void);
int check_gpio(void);
int check_gpio_sysfs(void);
int check_gpio_mmap(void);

struct pruss_test pruss_tests[] =
  {
    {"data_mem", prgm_42, sizeof (prgm_42), NULL, 0, check_data_mem},
    {"fibo", prgm_fibo, sizeof (prgm_fibo), NULL, 0, check_fibo},
    {"ackermann", prgm_acker, sizeof (prgm_acker), NULL, 0, check_acker},
    {"gpio_pru", prgm_gpio, sizeof (prgm_gpio), NULL, 0, check_gpio},
    {"gpio_sysfs", NULL, 0, NULL, 0, check_gpio_sysfs},
    {"gpio_mmap", NULL, 0, NULL, 0, check_gpio_mmap},
    {NULL, NULL, 0, NULL, 0, NULL}
  };

void init_pruss(void)
{
  tpruss_intc_initdata pruss_intc_initdata = PRUSS_INTC_INITDATA;
  prussdrv_init();

  for (int i = PRU_EVTOUT_0; i <= PRU_EVTOUT_7; ++i)
    if (prussdrv_open(i))
      errx(0, "prussdrv_open(%d) open failed", i);

  prussdrv_pruintc_init(&pruss_intc_initdata);
}

int main(int argc, char *argv[])
{
  init_pruss();
  if (argc == 1)
    {
      for (int i = 0; pruss_tests[i].id; ++i)
        pruss_exec(&pruss_tests[i]);
    }
  else
    {
      for (int i = 0; pruss_tests[i].id; ++i)
        if (strcmp(argv[1], pruss_tests[i].id) == 0)
          pruss_exec(&pruss_tests[i]);
    }
  prussdrv_exit();
  LOG("Done !\n");
  return 0;
}
