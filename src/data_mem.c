/*
** 42.c for BBB_PRUSS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Mon Feb  3 13:32:10 2014 Pierre Surply
** Last update Tue Feb  4 14:14:36 2014 Pierre Surply
*/

#include <prussdrv.h>
#include <pruss_intc_mapping.h>

#include "test.h"

int check_data_mem(void)
{
  char *mem;

  LOG("Waiting interrupt\n");
  prussdrv_pru_wait_event(PRU_EVTOUT_0);
  prussdrv_pru_clear_event(PRU_EVTOUT_0, PRU0_ARM_INTERRUPT);

  prussdrv_map_prumem(PRUSS0_PRU0_DATARAM, (void *) &mem);

  if (mem[0x1337] == 42)
    return 0;
  return 1;
}
