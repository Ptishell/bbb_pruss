/*
** exec.c for BBB_PRUSS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Mon Feb  3 12:57:13 2014 Pierre Surply
** Last update Mon Feb  3 13:41:32 2014 Pierre Surply
*/

#include <prussdrv.h>
#include <pruss_intc_mapping.h>

#include "test.h"

static void pruss_enable(const struct pruss_test *test)
{
  if (test->prgm_pru0)
      prussdrv_pru_enable(0);
  if (test->prgm_pru1)
      prussdrv_pru_enable(1);
}

static void pruss_disable(const struct pruss_test *test)
{
  if (test->prgm_pru0)
      prussdrv_pru_disable(0);
  if (test->prgm_pru1)
      prussdrv_pru_disable(1);
}

static void load_prgm(const struct pruss_test *test)
{
  if (test->prgm_pru0)
    {
      LOG("Loading program on PRU0 (%d bytes)\n", test->prgm_pru0_size);
      prussdrv_pru_write_memory(PRUSS0_PRU0_IRAM, 0,
                                test->prgm_pru0, test->prgm_pru0_size);
    }
  if (test->prgm_pru1)
    {
      LOG("Loading program on PRU1 (%d bytes)\n", test->prgm_pru1_size);
      prussdrv_pru_write_memory(PRUSS0_PRU0_IRAM, 0,
                                test->prgm_pru1, test->prgm_pru1_size);
    }
}

void pruss_exec(const struct pruss_test *test)
{
  int ret;

  LOG("-- %s --\n", test->id);
  pruss_disable(test);
  load_prgm(test);
  LOG("Starting execution\n");
  pruss_enable(test);
  if (!(ret = test->check()))
    LOG("OK\n");
  else
    LOG("KO (%d)\n", ret);
  pruss_disable(test);
}
